import axios from 'axios';

const CHANNEL_ID = '2117480';
const API_KEY = 'O5NT8Q2O1S3MVMFU'

const sendDataToThingSpeak = async (data) => {
  const url = `https://api.thingspeak.com/update.json?api_key=${API_KEY}`;
  const postData = { field1: data };

  try {
    await axios.post(url, postData);
    console.log('Data sent to ThingSpeak');
  } catch (error) {
    console.error('Error sending data to ThingSpeak:', error);
  }
}

export default sendDataToThingSpeak;
