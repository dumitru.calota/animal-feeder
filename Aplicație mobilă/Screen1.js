import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, ImageBackground } from 'react-native';
import axios from 'axios';

const Screen1 = () => {
  const [data, setData] = useState([]);
  const [foodData, setFoodData]= useState('');
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          'https://api.thingspeak.com/channels/2117480/feeds.json?api_key=ZC3BRS9HNI3LH12A&results=1'  // API ThingSpeak
        );
        setData(response.data.feeds); //actualizare "data" cu raspunsul de la API
        const feed = response.data.feeds[0];
        setFoodData(feed.field3); // actualizare "foodData" cu valoarea câmpului 3 
      } catch (error) {
        console.log('Error ThingSpeak data', error);
      }
    };

    fetchData();
    const interval = setInterval(() => {
      fetchData();       // apelarea functiei fetchData la fiecare 3 secunde pentru actualizarea datelor
    }, 3000);
    return () => clearInterval(interval);

  }, []);

  if (!foodData) {
    return <Text>Loading...</Text>;
  }

  return (
    <ImageBackground
      source={require('./assets/fundal.jpg')}
      style={styles.backgroundImage}
    >
      <View style={styles.container}>
        <View style={styles.row}>
          <Text style={styles.label}></Text> 
          <Text style={styles.value}>
            {data.length > 0 ? (<FoodLevel distance={data[0].field3} />) : ('Loading...')}   
          </Text>
        </View>
      </View>
    </ImageBackground>
  );
};

const FoodLevel = ({ distance }) => {
  console.log('Distance:', distance);
  if (distance >= 16) {
    return <Text style={styles.empty}>Food level: empty</Text>;   // Nivelul de hrană este gol sau insuficient încât să mai curgă
  } else if (distance >= 13 && distance < 16) {
    return <Text style={styles.low_level}>Food level: low level</Text>; //  Nivelul de hrană este aproape gol
  } else if (distance >= 9 && distance < 13) {
    return <Text style={styles.medium}>Food level: medium</Text>;  //  Nivelul de hrană este la jumatate
  } else if (distance > 4 && distance < 9) {
    return <Text style={styles.semi_full}>Food level: semi-full</Text>; //  Nivelul de hrană este aproape plin
  } else if (distance <= 4) {
    return <Text style={styles.full}>Food level: full</Text>; //  Nivelul de hrană este plin
  }
};

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 100, 
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  label: {
    fontSize: 64,
    fontWeight: 'bold',
    marginRight: 10,
    marginVertical: 60,
  },
  value: {
    fontSize: 30,
  },
  empty: {
    color: 'red',
    fontWeight: 'bold',
  },
  low_level: {
    color: 'orange',
    fontWeight: 'bold',
  },
  medium: {
    color: 'yellow',
    fontWeight: 'bold',
  },
  semi_full: {
    color: '#99ff66',
    fontWeight: 'bold',
  },
  full: {
    color: 'green',
    fontWeight: 'bold',
  },
});

export default Screen1;
