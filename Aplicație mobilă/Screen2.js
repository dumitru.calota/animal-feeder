import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, ImageBackground } from 'react-native';
import axios from 'axios';

const Screen2 = () => {
  const [data, setData] = useState([]);
  const [temperatureData, setTemperatureData] = useState('');
  const [humidityData, setHumidityData] = useState('');
  const [dataTime, setDataTime] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          'https://api.thingspeak.com/channels/2117480/feeds.json?api_key=ZC3BRS9HNI3LH12A&results=1' // API ThingSpeak
        );
        const feed = response.data.feeds[0]; //actualizare "data" cu raspunsul de la API
        setData(response.data.feeds);              
        setTemperatureData(feed.field1);  // actualizare date cu valoarea câmpurilor predestinate
        setHumidityData(feed.field2);
        setDataTime(feed.created_at);
      } catch (error) {
        console.log('Error ThingSpeak data', error);
      }
    };

    fetchData();
    const interval = setInterval(() => {
      fetchData();
    }, 3000);
    return () => clearInterval(interval);
  }, []);

  if (!temperatureData || !humidityData) {
    return <Text>Loading...</Text>;
  }

  const finalTime = new Date(dataTime).toLocaleTimeString([], {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
  });

  return (
    <ImageBackground
      source={require('./assets/fundal2.jpg')}
      style={styles.backgroundImage}
    >
      <View style={styles.container}>
        <View style={styles.row}>
          <Text style={styles.label}>Temperature:</Text>
          <Text style={styles.value}>
            {data.length > 0 ? `${parseFloat(data[0].field1).toFixed(1)}°C` : 'Loading...'}  
          </Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.label}>Humidity:</Text>
          <Text style={styles.value}>
            {data.length > 0 ? `${parseFloat(data[0].field2).toFixed(0)}%` : 'Loading...'}
          </Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.label}>Data received at:</Text>
          <Text style={styles.value}>{finalTime}</Text>
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 100,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  label: {
    fontSize: 20,
    fontWeight: 'bold',
    marginRight: 10,
  },
  value: {
    fontSize: 20,
  },
});

export default Screen2;
