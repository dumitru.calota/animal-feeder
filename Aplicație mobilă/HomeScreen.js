import React, { useState } from 'react';
import { View, Text, StyleSheet, ImageBackground } from 'react-native';
import { Button } from 'react-native-elements';
import Modal from 'react-native-modal';

const HomeScreen = ({ navigation }) => {
  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  return (
    <ImageBackground
      source={require('./assets/fundal3.jpg')}
      style={styles.backgroundImage}
    >
      <View style={styles.container}>
      <View style={styles.titleContainer}>
        
        <Text style={styles.title}>Animal Feeder</Text>
         </View>
        <Button
          title="Food level"
          onPress={() => navigation.navigate('Screen1')}
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.button}
        />
        <Button
          title="Temperature and Humidity"
          onPress={() => navigation.navigate('Screen2')}
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.button}
        />
        <Button
          title="Help"
          onPress={toggleModal}
          containerStyle={styles.buttonContainer}
          buttonStyle={styles.button}
        />

<Modal isVisible={isModalVisible} onBackdropPress={toggleModal}>  
  <View style={styles.modalContainer}>
    <Text style={styles.modalTitle}>How to use the application?</Text>
    <Text style={styles.modalText}>
      1. The "Food level" button displays the available food level for the animals.
    </Text>
    <Text style={styles.modalText}>
      2. The "Temperature and Humidity" button displays temperature and humidity in real time. 
    </Text>
    <Text style={styles.modalText}>
      3. Press the "Help" button to view app instructions.
    </Text>
  </View>
</Modal>

      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  titleContainer: {
    position: 'absolute',
    top: 109,
    alignItems: 'center',
    width: '100%',
  },
  title: {
    fontSize: 44,
    fontWeight: 'bold',
    color: '#00cc99',
  },
  buttonContainer: {
    width: '80%',
    marginVertical: 16,
  },
  button: {
    backgroundColor: '#0099FF',
  },
  modalContainer: {
    backgroundColor: '#99CCFF',
    padding: 16,
    borderRadius: 8,
    alignItems: 'center',
  },
  modalTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  modalText: {
    fontSize: 16,
    marginBottom: 8,
  },
});


export default HomeScreen;
