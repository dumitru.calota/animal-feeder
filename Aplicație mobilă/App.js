import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './HomeScreen';
import Screen1 from './Screen1';
import Screen2 from './Screen2';
import { enableScreens } from 'react-native-screens';

//enableScreens();

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    
    <NavigationContainer>
      
      
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: 'Home' }}
        />
        <Stack.Screen
          name="Screen1"
          component={Screen1}
          options={{ title: 'State of the food' }}
        />
        <Stack.Screen
          name="Screen2"
          component={Screen2}
          options={{ title: 'Environment stats' }}
        />
        

      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

