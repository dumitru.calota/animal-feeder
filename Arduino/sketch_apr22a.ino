#include "DHT.h"
#include <Adafruit_Sensor.h>
#include <SoftwareSerial.h>
#define DHTPIN 2
#include <ArduinoJson.h>
 #include "HX711.h"

#define DHTTYPE DHT11 
#include <avr/wdt.h>
#include <Servo.h>

Servo myservo; 

SoftwareSerial Arduino_SoftSerial(5, 6);// RX TX
DHT dht(DHTPIN, DHTTYPE);
int pos = 0;    
String str;
float h;
float t;
int trigPin = 10;    // Trigger
int echoPin = 13;    // Echo
long duration, cm, inches;


#define LOADCELL_DOUT_PIN  8
#define LOADCELL_SCK_PIN  11

HX711 scale;


void setup(){
  
Serial.begin(9600);
dht.begin();
Arduino_SoftSerial.begin(9600);
delay(2000);

myservo.write(0);
myservo.attach(9);

pinMode(DHTPIN,INPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

 scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
scale.set_scale(438.912);
scale.tare();               // reset the scale to 0

 
Serial.println("Program started");

}
void loop()
{
  StaticJsonDocument<1000> doc;

  // Read DHT11 sensor data
  DTH11_func();


  // Read HX711 weight sensor data
  HX711_func();

  // Read HC-SR04 distance sensor data
  HC_SR04_func();

  doc["humidity"] = h;
  doc["temperature"] = t;
  doc["cm"]=cm;

 // Serialize JSON document and send it on TX pin
 serializeJson(doc, Arduino_SoftSerial);

delay(15000); // Delay between measurements
}


void DTH11_func(){
 h = dht.readHumidity();
  t = dht.readTemperature();
  Serial.print("Humidity: ");
  Serial.println(h);
  Serial.print("Temperature: ");
  Serial.println(t);

 
}

void HX711_func(){

  float weight = scale.get_units();
  delay(1000);

  Serial.print("Weight: ");
  Serial.println(weight, 1); 

 if (weight >= 1000) {
   servoMotor();
 }

}

void HC_SR04_func(){

  digitalWrite(trigPin,LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin,LOW);


pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
 
  // Convert the time into a distance
  cm = (duration/2) / 29.1;    
 Serial.print("Distance: " );
   Serial.print(cm);
  Serial.print("cm");
  Serial.println();


}
void servoMotor(){

   for (pos = 0; pos <= 180; pos += 1) {
    myservo.write(pos);              
    delay(15);                      
  }
  for (pos = 180; pos >= 0; pos -= 1) {
    myservo.write(pos);
    delay(15);
  }}




