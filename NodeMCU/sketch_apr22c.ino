#include "DHT.h"
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <ThingSpeak.h>
#include <ArduinoJson.h>


const char* apiKey = "O5NT8Q2O1S3MVMFU";
unsigned long Channel_ID = 2117480;
const char* server = "api.thingspeak.com";

const int Field_Number_1 = 1;
const int Field_Number_2 = 2;
const int Field_Number_3 = 3;

boolean state1 = false;
boolean state2 = false;

const char *ssid = "Vali";
const char *pass = "12345678";

SoftwareSerial NodeMcu_SoftSerial(D6, D5); // RX TX

WiFiClient client;
float h, t, cm;


void setup()
{
Serial.begin(115200);
NodeMcu_SoftSerial.begin(9600);
ThingSpeak.begin(client);

while (!Serial) {
continue; // wait for serial port to connect
}
delay(2000);
Serial.println("Program Started");
wifi_connect();
}
void loop()
{

    if (WiFi.status() != WL_CONNECTED) {
      wifi_connect();
     
    }
    else {
     
      Serial.println("Currently Connected to WiFi");
    }

    //Connect to Thingspeak and push data
    while (state1 == false) {

      while (state2 == false) {
        parseJsonObject();
      }
      
      cloud_connect();
    }

  // Reset states to allow the sending process to repeat in the next loop
 
    state1 = false;
    state2 = false;
    Serial.println("Cloud Connect Succesful");
    Serial.println("-----------------------------------------");


}


void wifi_connect() {
  
  Serial.println("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, pass);

  // Wait for WiFi connection to be established
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  ThingSpeak.begin(client);
  Serial.println("WiFi Connected");

}

void cloud_connect() {
   // Connect to the Thingspeak server and send data
  if (client.connect(server, 80)) {
    String postStr = apiKey;
    postStr += "&field1=";
    postStr += String(t);
    postStr += "&field2=";
    postStr += String(h);
    postStr += "&field3=";
    postStr += String(cm);
    postStr += "\r\n\r\n";

// Build the HTTP POST request and send it to the server
    client.print("POST /update HTTP/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print(apiKey);
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(postStr.length());
    client.print("\n\n");
    client.print(postStr);

    Serial.print("Temperature: ");
    Serial.print(t);
    Serial.print(" || Humidity: ");
    Serial.println(h);
    Serial.print(cm);
    Serial.print("cm");
    Serial.println();
   

    // Set the field values for sending to Thingspeak
    ThingSpeak.setField(1, t);
    ThingSpeak.setField(2, h);
    ThingSpeak.setField(3,cm);

     Serial.println("Sent to Thingspeak");
     
    state1 = true;
   
 
    int x = ThingSpeak.writeFields(Channel_ID, apiKey);
  delay(15000);
  }
}
 

void parseJsonObject() {

 StaticJsonDocument<1000> doc;

    // Deserialize JSON from the serial port and store it in the JSON document
    DeserializationError error = deserializeJson(doc, NodeMcu_SoftSerial);



   // Check if deserialization is successful
 if (error) {
    Serial.println("deserializeJson error");
    return;
 }


   // Extract values from the JSON document and store them in respective variables
  h = doc["humidity"];
  t = doc["temperature"];
  cm=doc["cm"];

  // Check if the values are valid
  if (isnan(h) || isnan(t) || isnan(cm)|| h == 0.0 || t == 0.0 || cm == 0.0  ) {
    Serial.println("Invalid Sensor Readings");
    return;
  }

  state2 = true;
  Serial.println("JSON Object Recieved");

}